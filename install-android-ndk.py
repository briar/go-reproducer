#!/usr/bin/env python3
import os
from shutil import move, rmtree
from subprocess import check_call

from utils import get_build_versions, get_sha256, fail, get_version_and_tool, NDK_DIR


def main():
    # get tool name and version from command line parameters
    tool, command_line_version = get_version_and_tool()

    # Get the defined Android NDK version
    tool_version, versions = get_build_versions(tool, command_line_version)
    ndk = versions['ndk']
    print("Installing Android NDK %s for %s %s" % (ndk['revision'], tool, tool_version))

    if os.path.isdir(NDK_DIR):
        # check that we are using the correct NDK
        from configparser import ConfigParser
        config = ConfigParser()
        with open(os.path.join(NDK_DIR, 'source.properties'), 'rt') as f:
            config.read_string('[default]\n' + str(f.read()))
            revision = config.get('default', 'Pkg.Revision')

        if revision != ndk['revision']:
            print("Existing Android NDK has unexpected revision (%s). Deleting..." % revision)
            rmtree(NDK_DIR)

    if not os.path.isdir(NDK_DIR):
        # download Android NDK
        print("Downloading Android NDK...")
        check_call(['wget', '-c', '--no-verbose', ndk['url'], '-O', 'android-ndk.zip'])

        # check sha256 hash on downloaded file
        is_hash = get_sha256('android-ndk.zip')
        if is_hash != ndk['sha256']:
            fail("Android NDK checksum does not match, is %s" % is_hash)

        # install the NDK
        print("Unpacking Android NDK...")
        ndk_dir_tmp = NDK_DIR + '-tmp'
        check_call(['unzip', '-q', 'android-ndk.zip', '-d', ndk_dir_tmp])
        content = os.listdir(ndk_dir_tmp)
        if len(content) == 1 and content[0].startswith('android-ndk-r'):
            move(os.path.join(ndk_dir_tmp, content[0]), NDK_DIR)
            os.rmdir(ndk_dir_tmp)
        else:
            fail("Could not extract NDK: %s" % str(content))


if __name__ == "__main__":
    main()
