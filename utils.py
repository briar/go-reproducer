import hashlib
import json
import os
import sys
from collections import OrderedDict
from subprocess import check_call, check_output

# WARNING: changing those affects reproducibility
GO_ROOT = '/tmp/go'
GO_PATH = '/tmp/go-path'

NDK_DIR = 'android-ndk'


def get_version_and_tool():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        fail("Usage: %s <binary> [version]" % sys.argv[0])
    tool = sys.argv[1]
    command_line_version = sys.argv[2] if len(sys.argv) > 2 else None
    return tool, command_line_version


def get_build_versions(tool, tag):
    # load versions and their dependencies
    with open('versions.json', 'r') as f:
        versions = json.load(f, object_pairs_hook=OrderedDict)

    if tag is None:
        # take top-most version
        tag = next(iter(versions[tool]))
    versions[tool][tag]['tag'] = tag
    return tag, versions[tool][tag]

def get_ld_flags(versions):
    return versions['ld_flags'] if 'ld_flags' in versions else ''

def check_go_version(versions):
    # Check if proper Go version is installed (trailing space, because 'go1.10' in 'go1.10.1')
    if versions['go']['version'] + ' ' not in check_output(['go', 'version']).decode("UTF-8"):
        fail("You need Go version %s to reproduce this binary" % versions['go']['version'])


def ex(args, env=None, cwd=None):
    print("+ %s" % " ".join(args))
    check_call(args, env=env, cwd=cwd)


def fail(msg=""):
    sys.stderr.write("Error: %s\n" % msg)
    sys.exit(1)


def get_sha256(filename, block_size=65536):
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()


def reset_time(filename, versions):
    if 'timestamp' in versions: timestamp = versions['timestamp']
    else: timestamp = '197001010000.00'
    check_call(['touch', '--no-dereference', '-t', timestamp, filename])


def zip_files(files, zip_name, versions):
    for file_name in files:
        reset_time(file_name, versions)
        # use deterministic permissions to prevent differences in zip files
        os.chmod(file_name, 0o755)
    (zip_dir, zip_name) = os.path.split(zip_name)
    files = list(map(lambda f : os.path.relpath(f, zip_dir), files))
    ex(['zip', '-D', '-X', zip_name] + files, cwd=zip_dir)


def get_version_number(versions):
    return versions['tag']


def get_output_file_path(tool, versions, platform, suffix):
    output_dir = get_platform_output_dir(tool, platform)
    version = get_version_number(versions)
    filename = '%s-%s-%s%s' % (tool, platform, version, suffix)
    return os.path.join(output_dir, filename)


def get_final_file_path(tool, versions, platform):
    return get_output_file_path(tool, versions, platform, '.jar')


def get_sources_file_path(tool, versions, platform):
    return get_output_file_path(tool, versions, platform, '-sources.jar')


def get_pom_file_path(tool, versions, platform):
    return get_output_file_path(tool, versions, platform, '.pom')


def get_output_dir(tool):
    return os.path.abspath(os.path.join('output', tool))


def get_platform_output_dir(tool, platform):
    return os.path.abspath(os.path.join('output', tool, platform))
