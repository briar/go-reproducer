#!/usr/bin/env python3
import os
from subprocess import check_call

from utils import GO_ROOT, get_build_versions, ex, get_sha256, fail, check_go_version, \
    get_version_and_tool

URL = 'https://golang.org/dl/%s.src.tar.gz'


def main():
    # get tool name and version from command line parameters
    tool, command_line_version = get_version_and_tool()

    # Get the defined Go version
    tool_version, versions = get_build_versions(tool, command_line_version)
    print("Installing %s for %s %s" % (versions['go']['version'], tool, tool_version))

    if not os.path.isfile(os.path.join(GO_ROOT, 'bin', 'go')):
        # download Go source archive
        archive = 'go.tar.gz'
        url = URL % versions['go']['version']
        ex(['wget', '-c', '--no-verbose', url, '-O', archive])

        # check sha256 hash on downloaded file
        is_hash = get_sha256(archive)
        if is_hash != versions['go']['sha256']:
            fail("Go checksum does not match, is %s" % is_hash)

        # unpack Go source archive
        ex(['tar', '-C', os.path.dirname(GO_ROOT), '-xzf', archive])

        # build
        check_call(['./make.bash'], cwd=os.path.join(GO_ROOT, 'src'))

    # final version check
    go_bin_path = os.path.join(GO_ROOT, 'bin')
    os.environ['PATH'] = go_bin_path + os.pathsep + os.getenv('PATH')
    check_go_version(versions)


if __name__ == "__main__":
    main()
