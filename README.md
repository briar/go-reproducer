# Go Reproducer

This is a tool you can use to verify that golang binaries
used by [Briar](https://briarproject.org) (such as obfs4proxy and snowflake)
were built exactly from the public source code
and no modifications (such as backdoors) were added.

Current packages:

* https://mvnrepository.com/artifact/org.briarproject/obfs4proxy-android
* https://mvnrepository.com/artifact/org.briarproject/obfs4proxy-linux
* https://mvnrepository.com/artifact/org.briarproject/obfs4proxy-windows
* https://mvnrepository.com/artifact/org.briarproject/snowflake-android
* https://mvnrepository.com/artifact/org.briarproject/snowflake-linux
* https://mvnrepository.com/artifact/org.briarproject/snowflake-windows

More information about these so called reproducible builds is available at
[reproducible-builds.org](https://reproducible-builds.org/).

The source code for this tool is available at
https://code.briarproject.org/briar/go-reproducer

## How to use

Make sure the version of Go you want to verify is included in `versions.json`.

Verify that you have `docker` installed:

    docker --version

If this command does not work,
please [install Docker](https://docs.docker.com/install/)
and continue once it is installed.

Note: While not guaranteed to work,
it *might* also be possible to perform the steps below
without Docker on a Debian stable system.

### Using our pre-built image

If you trust that our pre-built Docker image was built exactly from *its* source,
you can use it for faster verification.
If not, you can read the next section to learn how to build the image yourself.
Then you are only trusting the official `debian:stable` image which is out of our control.

Otherwise, you can skip the next section and move directly to *Run the verification*.

### Building your own image

Check out the source repository:

    git clone https://code.briarproject.org/briar/go-reproducer.git

Build our Docker image:

    docker build -t briar/go-reproducer go-reproducer

To ensure reproducibility we build the image from a fixed snapshot of Debian
and ignore expiry warnings for Debian packages. This means the build process
inside the Docker container may use outdated packages that could contain
known vulnerabilities.

### Run the verification

To verify a specific version of a binary, run

    docker run briar/go-reproducer:latest ./verify-binary.py <binary> [version]

Where `<binary>` is either `obfs4proxy` or `snowflake` and `[version]` is the version of the binary
you want to test, for example `0.0.14-tor1`.

You can find a list of versions in `versions.json`. These are based on tags in the source code
repositories for
[obfs4proxy](https://gitweb.torproject.org/pluggable-transports/obfs4.git/refs) and
[snowflake](https://gitweb.torproject.org/pluggable-transports/snowflake.git/refs).

If you leave out `[version]` it will build the latest version
that was registered in `versions.json`.

In case there is an issue with the verification of an old build,
this *might* be caused by an update of the container.
You can try to use the original container by running:

    docker run briar/go-reproducer:[version] ./verify-binary.py <binary> [version]

There should be a tag with the name `[version]` in this repository
that you could be used to reproduce the old container.
Note that this will not work if the issue is caused by an updated Debian package.

### Only build the binary

To build a specific version of a binary without verifying it, run

    docker run briar/go-reproducer:latest ./build-binary.py <binary> [version]
