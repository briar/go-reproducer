#!/usr/bin/env python3
import os
import sys
from subprocess import check_call, CalledProcessError

from utils import get_sha256, fail, get_build_versions, get_final_file_path, \
    get_version_and_tool, get_version_number


def main():
    # get version from command or show usage information
    tool, command_line_version = get_version_and_tool()

    verified_android = verify(tool, command_line_version, 'android')
    verified_linux = verify(tool, command_line_version, 'linux')
    verified_windows = verify(tool, command_line_version, 'windows')
    verified_macos = verify(tool, command_line_version, 'macos')
    if verified_android and verified_linux and verified_windows and verified_macos:
        sys.exit(0)
    else:
        sys.exit(1)


def verify(tool, command_line_version, platform):
    # get version and versions of its dependencies
    tool_version, versions = get_build_versions(tool, command_line_version)

    # download reference binary
    file_name = get_final_file_path(tool, versions, platform)
    os.makedirs('reference', exist_ok=True)
    reference_file_name = os.path.join('reference', os.path.basename(file_name))
    # try downloading from maven central
    check_call(['wget', '--no-verbose', get_url(tool, versions, platform), '-O',
                reference_file_name])

    # check if it was already build
    if not os.path.isfile(file_name):
        # build it first
        if command_line_version is None:
            check_call(['./build-binary.py', tool])
        else:
            check_call(['./build-binary.py', tool, command_line_version])

    # calculate hashes for both files
    reference_hash = get_sha256(reference_file_name)
    build_hash = get_sha256(file_name)
    print("Reference sha256: %s" % reference_hash)
    print("Build sha256:     %s" % build_hash)

    # compare hashes
    if reference_hash == build_hash:
        print("%s-%s version %s was successfully verified! \o/" % (tool, platform, tool_version))
        return True
    else:
        print("Hashes for %s-%s version %s do not match! :(" % (tool, platform, tool_version))
        return False


def get_url(tool, versions, platform):
    version = get_version_number(versions)
    directory = "%s-%s" % (tool, platform)
    file = os.path.basename(get_final_file_path(tool, versions, platform))
    return "https://repo.maven.apache.org/maven2/org/briarproject/%s/%s/%s" % (directory, version, file)


if __name__ == "__main__":
    main()
