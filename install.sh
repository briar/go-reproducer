#!/usr/bin/env bash
set -e
set -x

# use snapshot repos for deterministic package versions
DATE="20240301T000000Z"
cat << EOF > /etc/apt/sources.list
deb http://snapshot.debian.org/archive/debian/${DATE}/ bookworm main
deb http://snapshot.debian.org/archive/debian/${DATE}/ bookworm-updates main
EOF

# ignore expired package releases so we can reproduce from old snapshots
echo 'Acquire::Check-Valid-Until "0";' >> /etc/apt/apt.conf.d/10-ignore-expiry

# update package sources
apt-get update
apt-get -y upgrade

# do not install documentation to keep image small
echo "path-exclude=/usr/share/locale/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc
echo "path-exclude=/usr/share/man/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc
echo "path-exclude=/usr/share/doc/*" >> /etc/dpkg/dpkg.cfg.d/01_nodoc

# install dependencies
./install-dependencies.sh

# clean up for smaller image size
apt-get -y autoremove --purge
apt-get clean
rm -rf /var/lib/apt/lists/*
