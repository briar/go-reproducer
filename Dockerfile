FROM debian:bookworm

ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /opt/go-reproducer

ADD build-binary.py ./
ADD install*.py ./
ADD install*.sh ./
ADD versions.json ./
ADD utils.py ./
ADD template-*.pom ./
ADD verify-binary.py ./

RUN ./install.sh

CMD ./verify-binary.py obfs4proxy
