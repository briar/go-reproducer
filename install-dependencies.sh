#!/usr/bin/env bash
set -e
set -x

apt-get install -y --no-install-recommends \
	ca-certificates \
	build-essential \
	git \
	zip \
	unzip \
	wget \
	fastjar \
	python3 \
	golang
